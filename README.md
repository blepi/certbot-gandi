# certbot-gandi
Extend Certbot docker image to obtain Let's Encrypt certificates using DNS Challenge with GANDI.

Notes: made for my personal use, use at your own risk. Feedbacks are welcome.

## Build docker image locally

docker build . -t certbot-gandi:latest

## Generating a certificate using DNS Challenge

Notes: you don't need the e-mail to be valid to generate the certificate. Command below will generate one certificates valid for example.com, *.example.com, and *.fwd.example.com.

IMPORTANT: certificates and let's encrypt account data will be stored in `$HOME/webdata/letsencrypt`.

The GandiV5 API key is passed as the environment variable `GANDI_API_KEY` to the container (flag `-e` below).

```
docker run --rm -v$HOME/webdata/letsencrypt:/etc/letsencrypt/ -e GANDI_API_KEY=MYSECRETAPIKEY registry.gitlab.com/blepi/certbot-gandi:latest certonly --authenticator dns-gandi --dns-gandi-credentials /app/gandi.ini --agree-tos -m certbot@example.com --non-interactive -d *.example.com -d example.com -d *.fwd.example.com
```

Output:
``` 
latest: Pulling from blep/certbot-gandi
Digest: sha256:dbe4b46f7fdddbd443ddecef67dae1a37abbd0eea7e19d57fd6998013ea9b0fb
Status: Image is up to date for blep/certbot-gandi:latest
docker.io/blep/certbot-gandi:latest
Writing GANDI_API_KEY environment variable to /app/gandi.ini
Running certbot certonly --authenticator dns-gandi --dns-gandi-credentials /app/gandi.ini --agree-tos -m certbot@example.com --non-interactive -d *.example.com -d example.com -d *.fwd.example.com
Account registered.
Requesting a certificate for *.example.com and 2 more domains
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Missing property in credentials configuration file /app/gandi.ini:
 * Property "dns_gandi_api_key" not found (should be API key for Gandi account).
Ask for help or search for solutions at https://community.letsencrypt.org. See the logfile /var/log/letsencrypt/letsencrypt.log or re-run Certbot with -v for more details.
```

The script automatically detects if the certificate needs to be updated. Easiest is to run it in a daily cron job.

## Securing you API key

A good article on options available to secure you GANDI API key:
https://www.eff.org/deeplinks/2018/02/technical-deep-dive-securing-automation-acme-dns-challenge-validation

The easiest is just to not run the cron job on a machine you consider secure to store the GANDI API key.

## Related projects

- https://github.com/obynio/certbot-plugin-gandi
- https://hub.docker.com/r/certbot/certbot/dockerfile
- https://github.com/certbot/certbot
- https://letsencrypt.org/
- https://community.letsencrypt.org/t/acme-v2-production-environment-wildcards/55578

